# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('menuname', 'image_mini')
    def image_mini(self, obj):
        if obj.image:
            ret = u'<img style="max-height: 200px; max-width: 200px;" src="%s"/>' % obj.image.url
        else:
            ret = u'<b>Ops! There is no preview available</b>'
        return ret
    image_mini.allow_tags = True

class PasantiaContentInlineAdmin (admin.TabularInline):
    model = models.PasantiasContent
    extra = 0

@admin.register(models.Pasantias)
class PasantiasAdmin(admin.ModelAdmin):
    list_display = ('title', )
    inlines = [PasantiaContentInlineAdmin, ]

class PublicacionContentInlineAdmin (admin.TabularInline):
    model = models.PublicacionContent
    extra = 0

@admin.register(models.Publicaciones)
class PublicacionesAdmin(admin.ModelAdmin):
    list_display = ('title', )
    inlines = [PublicacionContentInlineAdmin, ]