from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = (
    url(r'^$', views.index, name='welcome'),
    url(r'^pacomarca/$', views.predirect, name='redirect'),
    url(r'^pacomarca/informacion-basica/$', views.info, name='info'),
    url(r'^(\w+)/$', views.pagecontent1),
    url(r'^(\w+)/(\w+)/$', views.pagecontent2),
)
