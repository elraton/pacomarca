# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from web.models import *

# Create your views here.
def index(request):
    context = {
        'Events' : 'Events',
    }
    return render(request, 'index.html', {'context': context})

def about(request):
    context = {
        'Events' : 'Events',
    }
    return render(request, 'about.html', {'context': context})

def info(request):
    context = {
        'Events' : 'Events',
    }
    return render(request, 'info.html', {'context': context})

def pagecontent1(request, page):
    page = Page.objects.get(menuname='/' + page + '/')
    context = {
        'content': page,
    }
    return render(request, 'page.html', {'context': context})

def predirect(request):
    return redirect('/pacomarca/sobrenosotros/')

def pagecontent2(request, page, subpage):
    page = Page.objects.get(menuname='/' + page + '/' + subpage + '/')
    if subpage == 'visitas':
        pasantias = Pasantias.objects.all()
        print pasantias
        context = {
            'content': page,
            'pasantias': pasantias
        }
    else:
        context = {
            'content': page,
        }
    return render(request, 'page.html', {'context': context})