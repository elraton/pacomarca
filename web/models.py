# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.
class Page(models.Model):
    menuname = models.CharField(max_length=50, verbose_name='Nombre en el Menu')
    image    = models.ImageField(upload_to='pages', verbose_name='Imagen', blank=True, null=True)
    text = RichTextField(verbose_name='Texto', blank=False)


    def __unicode__(self):
        return self.menuname

    class Meta:
        verbose_name = 'Pagina de Contenido'
        verbose_name_plural = 'Pagina de Contenido'

class Pasantias(models.Model):
    title = models.CharField(max_length=50, verbose_name='Titulo')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Pasantias'
        verbose_name_plural = 'Pasantias'

class PasantiasContent(models.Model):
    title = models.CharField(max_length=50, verbose_name='Titulo')
    hour_start = models.TimeField(verbose_name='Hora Inicio')
    hour_end = models.TimeField(verbose_name='Hora Fin', null=True, blank=True)
    text = RichTextField(verbose_name='Descripción del programa', blank=True)
    pasantia = models.ForeignKey(Pasantias)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Pasantias programa'
        verbose_name_plural = 'Pasantias programa'


class Publicaciones(models.Model):
    title = models.CharField(max_length=50, verbose_name='Titulo')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Publicaciones'
        verbose_name_plural = 'Publicaciones'

class PublicacionContent(models.Model):
    title = models.CharField(max_length=50, verbose_name='Titulo')
    file = models.FileField(verbose_name='Archivo')
    publicacion = models.ForeignKey(Publicaciones)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Publicaciones link'
        verbose_name_plural = 'Publicaciones link'